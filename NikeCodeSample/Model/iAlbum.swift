//
//  iAlbum.swift
//  NikeCodeSample
//
//  Created by SM Moniruzaman on 1/23/20.
//  Copyright © 2020 SM Moniruzaman. All rights reserved.
//

import UIKit

class iAlbumWrapper:NSObject {
    var objects: [iAlbum]?
    init(jsonObject: Any?) {
        if let tempArr = jsonObject as? [[String:Any]] {
            self.objects = tempArr.compactMap({ iAlbum(json: $0)})
        }  else {
            self.objects = nil
        }
    }
}

struct iAlbum {

    var artistName:String
    var id:String
    var releaseDate:String
    var name:String
    var kind:String
    var copyright:String
    var artistId:String
    var contentAdvisoryRating:String
    var artistUrl:String
    var artworkUrl100:String
    var url:String
    var genres:[Genre]
 
    init(json: [String: Any]) {
        self.artistName = json["artistName"] as? String ?? ""
        self.id = json["id"] as? String ?? ""
        self.releaseDate = json["releaseDate"] as? String ?? ""
        self.name = json["name"] as? String ?? ""
        self.kind = json["kind"] as? String ?? ""
        self.copyright = json["copyright"] as? String ?? ""
        self.artistId = json["artistId"] as? String ?? ""
        self.contentAdvisoryRating = json["contentAdvisoryRating"] as? String ?? ""
        self.artistUrl = json["artistUrl"] as? String ?? ""
        self.artworkUrl100 = json["artworkUrl100"] as? String ?? ""
        self.url = json["url"] as? String ?? ""
        self.genres = GenreWrapper(jsonObject: json["genres"] as Any).objects ?? []
    }
}




class GenreWrapper:NSObject {
    var objects: [Genre]?
    init(jsonObject: Any?) {
        if let tempArr = jsonObject as? [[String:Any]] {
            self.objects = tempArr.compactMap({ Genre(json: $0)})
        }  else {
            self.objects = nil
        }
        super.init()
    }
}

struct Genre {
    
    var genreId:String
    var name:String
    var url:String
    
    init(json: [String: Any]) {
        self.genreId = json["genreId"] as? String ?? ""
        self.name = json["name"] as? String ?? ""
        self.url = json["url"] as? String ?? ""
    }

}
