//
//  UIImageView+Utilities.swift
//  NikeCodeSample
//
//  Created by SM Moniruzaman on 1/23/20.
//  Copyright © 2020 SM Moniruzaman. All rights reserved.
//

import UIKit

extension UIImageView {
      
    func setImageWithURL(urlString: String, contentMode mode: UIView.ContentMode = .scaleAspectFit,  completion:@escaping (() -> Void)) {
        guard let url = URL(string: urlString) else {
            self.image = UIImage(named: "defaultProfilePicture")
            completion()
            return
        }
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            DispatchQueue.main.async() {
                guard let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                    let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                    let data = data, error == nil,
                    let image = UIImage(data: data)
                    else {
                        self.image = UIImage(named: "defaultProfilePicture")
                        completion()
                        return
                }
                self.image = image
                completion()
            }
        }.resume()
    }
    
}
