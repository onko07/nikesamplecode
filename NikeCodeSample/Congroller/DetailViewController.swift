//
//  DetailViewController.swift
//  NikeCodeSample
//
//  Created by SM Moniruzaman on 1/24/20.
//  Copyright © 2020 SM Moniruzaman. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    var album:iAlbum?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        setupStackWithContents()
    }
    
    // Setup Stack Contents
    func setupStackWithContents() {
         
        let mainStack = UIStackView(frame: UIScreen.main.bounds)
        mainStack.alignment = .fill
        mainStack.axis = .vertical
        mainStack.spacing = 10
        
        view.addSubview(mainStack)
        mainStack.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint(item: mainStack, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 20).isActive = true
        NSLayoutConstraint(item: mainStack, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: -20).isActive = true
        NSLayoutConstraint(item: mainStack, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: 5).isActive = true
        NSLayoutConstraint(item: mainStack, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0).isActive = true
        
        mainStack.addArrangedSubview(addTopImageView())
        
        let contentStack = UIStackView()
        contentStack.spacing = 10
        contentStack.alignment = .fill
        contentStack.axis = .vertical
        
        contentStack.addArrangedSubview(addAlbumName())
        contentStack.addArrangedSubview(addArtistName())
        contentStack.addArrangedSubview(addGenras())
        contentStack.addArrangedSubview(addReleaseDate())
        contentStack.addArrangedSubview(addCopyRight())
        
        mainStack.addArrangedSubview(contentStack)
        
        addStoreButton()
    }
    
    // Adding Top ImageView
    func addTopImageView() -> UIImageView {
        let mainImageView = UIImageView()
        mainImageView.setImageWithURL(urlString: album?.artworkUrl100 ?? "") {}
        return mainImageView
    }
    
    // Adding Album Name
    func addAlbumName() -> UILabel {
        let albumName = UILabel()
        albumName.textColor = UIColor.black
        albumName.text = "Album Name: \(album?.name ?? "")"
        albumName.numberOfLines = 2
        return albumName
    }
    
    // Adding ArtistName
    func addArtistName() -> UILabel {
        let artistName = UILabel()
        artistName.textColor = UIColor.black
        artistName.text = "Artist Name: \(album?.artistName ?? "")"
        artistName.numberOfLines = 2
        return artistName
    }
    
    // Adding Genras
    func addGenras() -> UIStackView {
        let horizontalStack = UIStackView()
        horizontalStack.alignment = .leading
        horizontalStack.axis = .horizontal
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.regular)
        label.textColor = UIColor.black
        label.text = "Genre's"
        label.widthAnchor.constraint(equalToConstant: 70).isActive = true
        horizontalStack.addArrangedSubview(label)
        
        let genraStack = UIStackView()
        genraStack.alignment = .fill
        genraStack.axis = .vertical
        
        for item in album?.genres ?? [] {
            let genraLabel = UILabel()
            genraLabel.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.regular)
            genraLabel.textColor = UIColor.black
            genraLabel.text = item.name
            genraStack.addArrangedSubview(genraLabel)
        }
        
        horizontalStack.addArrangedSubview(genraStack)
        return horizontalStack
    }
    
    // Adding release Date
    func addReleaseDate() -> UILabel {
        let releaseDate = UILabel()
        releaseDate.text = "Release Date: \(album?.releaseDate ?? "")"
        releaseDate.textColor = UIColor.black
        return releaseDate
    }

    // Adding Copy RIght
    func addCopyRight() -> UILabel {
        let copyright = UILabel()
        copyright.textColor = UIColor.black
        copyright.numberOfLines = 0
        copyright.text = "Copyright: \(album?.copyright ?? "")"
        return copyright
    }
    
    // Adding Store Button
    func addStoreButton()  {
        let button = UIButton()
        button.setTitle("Store", for: .normal)
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        button.backgroundColor = UIColor.darkGray
        self.view.addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
    
        NSLayoutConstraint(item: button, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 20).isActive = true
        NSLayoutConstraint(item: button, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: -20).isActive = true
        NSLayoutConstraint(item: button, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: -20).isActive = true
    }
  
    
    @objc func buttonTapped() {
        
        if let url = URL(string: album?.url ?? ""),UIApplication.shared.canOpenURL(url) { // itunes URL can only open in Phne
            UIApplication.shared.open(url, options: [:], completionHandler: nil)        }
    }
}
