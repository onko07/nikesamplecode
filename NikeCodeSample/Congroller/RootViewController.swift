//
//  ViewController.swift
//  NikeCodeSample
//
//  Created by SM Moniruzaman on 1/23/20.
//  Copyright © 2020 SM Moniruzaman. All rights reserved.
//

import UIKit

class RootViewController: UITableViewController {
    
    let cellId = "cell"
    
    var albumArray : [iAlbum] = [] {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UINavigationBar.appearance().barTintColor = .white
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        UINavigationBar.appearance().isTranslucent = false
        
        title = "Albums"
        view.backgroundColor = UIColor.white
        setupTableView()
    }
    
    func setupTableView(){
        
        APIRequestManager.GetAlbums {[weak self] (albums) in
            if let strongSelf = self, let array = albums {
                strongSelf.albumArray = array
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albumArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: cellId)
        cell.textLabel?.textColor = UIColor.black
        cell.detailTextLabel?.textColor = UIColor.black
        cell.backgroundColor = UIColor.white
        let album = albumArray[indexPath.row]
        cell.textLabel?.text = album.name
        cell.detailTextLabel?.text = album.artistName
        cell.imageView?.setImageWithURL(urlString: album.artworkUrl100, completion: {
            cell.setNeedsLayout() //invalidate current layout
            cell.layoutIfNeeded() //update immediately
        })
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let detailVC = DetailViewController()
        detailVC.album = albumArray[indexPath.row]
        self.navigationController?.pushViewController(detailVC, animated: true)
        
    }
}
