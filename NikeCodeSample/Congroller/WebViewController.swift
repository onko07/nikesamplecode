//
//  WebViewController.swift
//  NikeCodeSample
//
//  Created by SM Moniruzaman on 1/24/20.
//  Copyright © 2020 SM Moniruzaman. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {

    var ituneStoreLink:String?
     
    lazy var webView: WKWebView = {
        let wv = WKWebView(frame: UIScreen.main.bounds, configuration: WKWebViewConfiguration())
        wv.uiDelegate = self
        wv.navigationDelegate = self
        return wv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(webView)
        guard let url = URL(string: ituneStoreLink ?? "") else { return }
        let request = URLRequest(url: url)
        webView.load(request)
    }
    
}

extension WebViewController: WKNavigationDelegate, WKUIDelegate {
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print(webView.url as Any)
        print("Started to load")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("finish to load")
    }

}
