//
//  AlbumRequestConfigurator.swift
//  NikeCodeSample
//
//  Created by SM Moniruzaman on 1/24/20.
//  Copyright © 2020 SM Moniruzaman. All rights reserved.
//

import Foundation

struct AlbumRequestConfigurator: RequestConfigurator {
 
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var host: String {
        return "rss.itunes.apple.com"
    }
    
    var httpProtocol: HTTPProtocol {
        return .http
    }
    
    var path: String {
        return "/api/v1/us/apple-music/coming-soon/all/100/explicit.json"
    }
      
}
