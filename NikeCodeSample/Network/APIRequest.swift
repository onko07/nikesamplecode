//
//  APIRequest.swift
//  NikeCodeSample
//
//  Created by SM Moniruzaman on 1/24/20.
//  Copyright © 2020 SM Moniruzaman. All rights reserved.
//

import Foundation

public final class APIRequest {
    private let configurator: RequestConfigurator
    
    public init(configurator: RequestConfigurator) {
        self.configurator = configurator
    }
    
    public func make(_ completion: @escaping (Any?, Error?) -> ()) {
        DispatchQueue.global(qos: .userInitiated).async {
            guard let request = self.configurator.buildRequest() else {
                completion(nil, NSError(domain: "Invalid URL Request", code: -1, userInfo: nil) as Error)
                return
            }
            APIClient.dataTaskExecute(request: request) { (data, response, error) in
                completion(self.makeModelFromJSON(data), nil)
            }
        }
    }
    
    private func makeModelFromJSON(_ data: Data?) -> Any? {
        guard let jsonData = data else { return nil }
        
        do {
            let jsonObject = try JSONSerialization.jsonObject(with: jsonData, options: [.allowFragments, .mutableLeaves, .mutableContainers]) as? [String:Any]
            return jsonObject
            
        } catch {
            //TODO: LOG..
            print(error)
        }
        
        return nil
    }
}
