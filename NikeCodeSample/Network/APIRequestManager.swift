//
//  APIRequestManager.swift
//  NikeCodeSample
//
//  Created by SM Moniruzaman on 1/24/20.
//  Copyright © 2020 SM Moniruzaman. All rights reserved.
//

import UIKit

class APIRequestManager: NSObject {
    
    static func GetAlbums(completion: @escaping (_ contacts: [iAlbum]?) -> Void)  {
        APIRequest(configurator: AlbumRequestConfigurator()).make { (respone, error) in
            DispatchQueue.main.async {
                if let dictionary = respone as? [String:Any],
                    let feed = dictionary["feed"] as? [String:Any],
                    let results = feed["results"] as? [[String:Any]] {
                    completion(iAlbumWrapper(jsonObject: results).objects)
                }
                else{
                    completion(nil)
                }
            }
        } 
    } 
}
