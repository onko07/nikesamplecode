//
//  APIClient.swift
//  NikeCodeSample
//
//  Created by SM Moniruzaman on 1/24/20.
//  Copyright © 2020 SM Moniruzaman. All rights reserved.
//

import Foundation

typealias APIHandler = (Data?, URLResponse?, Error?) -> Swift.Void
typealias DownloadHandler = (URL?, URLResponse?, Error?) -> Swift.Void

final class APIClient: NSObject {
    
    private static let shared = APIClient()
        
    private let session: URLSession
    
    private override init() {
        
        let configuration = URLSessionConfiguration.default
        configuration.requestCachePolicy = .reloadIgnoringCacheData
        
        self.session = URLSession(configuration: configuration)
        
        super.init()
    }
    
    class func dataTaskExecute(request: URLRequest, apiHandler: @escaping APIHandler) {
        let dataTask = APIClient.shared.session.dataTask(with: request, completionHandler: apiHandler)
        dataTask.resume()
    }
}
