//
//  RequestConfigurator.swift
//  NikeCodeSample
//
//  Created by SM Moniruzaman on 1/24/20.
//  Copyright © 2020 SM Moniruzaman. All rights reserved.
//

import Foundation

public enum HTTPMethod: String {
    case get
}

public enum HTTPProtocol: String {
    case http, https
}

public protocol RequestConfigurator {
    
    var baseURL: URL? { get }
    var httpMethod: HTTPMethod { get }
    var httpProtocol: HTTPProtocol { get }
    var host: String { get }
    var path: String { get } 
    
    var cachePolicy: URLRequest.CachePolicy { get }
    var timeoutInterval: TimeInterval { get }
    
    func buildRequest() -> URLRequest?
}

extension RequestConfigurator {
    
    var baseURL: URL? { 
        if let urlComponents = URL(string: self.httpProtocol.rawValue + "://" + self.host + self.path) {
             return urlComponents
        }
        return nil
    }
    
    var host: String {
        return "itunes.apple.com"
    }
       
    var cachePolicy: URLRequest.CachePolicy {
        return .reloadIgnoringLocalCacheData
    }
    
    var timeoutInterval: TimeInterval {
        return 60
    }
    
    func buildRequest() -> URLRequest? {
        //TODO: Check and throw
        guard let url = self.baseURL else { return nil}
        var request = URLRequest(url: url, cachePolicy: self.cachePolicy, timeoutInterval: self.timeoutInterval)
        request.httpMethod = self.httpMethod.rawValue
        return request
    }
}
