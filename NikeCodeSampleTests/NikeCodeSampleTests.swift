//
//  NikeCodeSampleTests.swift
//  NikeCodeSampleTests
//
//  Created by SM Moniruzaman on 1/23/20.
//  Copyright © 2020 SM Moniruzaman. All rights reserved.
//

import XCTest
@testable import NikeCodeSample

class NikeCodeSampleTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testAPIFor100Items() {
        
        let expectation = XCTestExpectation.init(description: "check 100 albmum")
        
        APIRequestManager.GetAlbums { (response) in
            if let albums = response, albums.count == 0 {
                expectation.fulfill()
            } else {
                XCTFail("Fail")
            }
        }
    } 

}
